package ru.maa.readdirectory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;


/**
 * Created by Anton on 22.03.2018
 */
public class ReadDirectory {

    private static final String word = wordenter() ;

    public static void main(String[] args) {
        File pathDirectory;
        String[] listDir;
        pathDirectory = new File(dataenter());
        listDir = pathDirectory.list();
        System.out.println(Arrays.toString(listDir));

        File[] files = pathDirectory.listFiles();
        assert files != null;
        for (File file : files) {
            if (file.getName().contains(".txt")) {
                readLine(file);
            }

        }
    }

    /**
     * Метод для ввода пути.
     */
    private static String dataenter() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите путь: ");
        return scanner.nextLine();
    }

    /**
     *Метод для ввода слова которые будем искать.
     */
    private static String wordenter() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите слово которые будем искать: ");
        return scanner.nextLine();
    }

    /**
     * Метод для поиска нужного слова.
     */
    private static void readLine(File file) {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            int counter = 1;
            while ((line = reader.readLine()) != null) {
                if (line.contains(word)) {
                    System.out.println("Нужное слово находится в " + file + " в строке под номером " + counter);
                }
                counter++;
            }
        } catch (IOException e) {
            System.err.println("ERROR");
        }
    }
}